# Rust Projects

## Description
Repository for practicing the Rust programming language via small projects with a cyber-security focus.

## Usage
Each project will include src/main.rs and other .rs files (if created). Cargo.lock and Cargo.toml will be provided to list dependencies.
Change directory into the folder required and use cargo run along with the correct program parameters.

## Programs

  - **hash_cracker_rs**: Cracks MD5, SHA1 and SHA256 hashes using a supplied wordlist.
  - **grss**: Prints lines in file which match a user-supplied pattern (string). 
  - **passphrase_rs**: Creates a passphrase using 360,000 line wordlist, with user args using clap crate
  - **uni_finder**: Makes an API request to find all universities in a country and city.
  - **rustfetch**: Program which collects basic user information about the system similiar to neofetch.
  - **integrity_rs**: Checks for files in a given folder and compares hashes to check if files within the folder have been modified or not. 
  
## License

