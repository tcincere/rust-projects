#![allow(unused)]

use clap::Parser;
use std::{
    fs::File,
    io::{BufRead, BufReader, Error},
};

//Derive the data from the parser.
// Works with structs, enums etc.
#[derive(Parser)]
struct Cli {
    // Adding help with '///'
    // Switch from positional to named parameters

    /// pattern to search for
    #[arg(short, long)]
    pattern: String,

    /// path to file
    #[arg(short, long)]
    file: std::path::PathBuf,
}

fn main() -> Result<(), Error> {
    // Parse the arguments
    let args: Cli = Cli::parse();

    // Open file and use buffer reader.
    let file = File::open(args.file)?;
    let reader = BufReader::new(file);

    // Iterate through each line and find match.
    for line in reader.lines() {
        let line = line?;

        if line.contains(&args.pattern) {
            println!("{}", line);
        }
    }

    Ok(())
}
