use clap::Parser;
use hex;
use serde_json;
use sha2::{Digest, Sha256};
use std::{
    collections::HashMap,
    default, fs,
    fs::File,
    io::{BufWriter, Read, Result, Write},
};

mod check_integrity;

#[derive(Parser)]
struct Arguments {
    ///Folder which contains files to check integrity
    #[arg(short, long)]
    folder: String,

    ///Absolute path to save JSON file
    #[arg(short, long)]
    output: Option<String>,

    /// Path to JSON file (opt.)
    #[arg(short, long)]
    json: Option<String>,

    /// Enable integrity checking (opt.)
    #[arg(short, long, default_value_t = false)]
    check: bool,
}

fn main() {
    // Collect user arguments
    let args: Arguments = Arguments::parse();

    let mut original_hashes = Default::default();
    let mut output_file = Default::default();

    match args.json {
        Some(json_file) => original_hashes = check_integrity::parse_json(json_file),
        None => {}
    }

    match args.output {
        Some(file) => output_file = file,
        None => {}
    }

    // Collect result. Check if supplied argument is valid directory.
    let is_directory: bool = check_if_directory_valid(args.folder.to_string());

    // Print an error if argument is not a directory.
    if !is_directory {
        eprintln!("[!] Error. Supplied argument is not valid.");
    }

    // Get list of all files
    let files = get_files_in_folder(args.folder);

    // Get list of file hashes
    let hashes = hash_files(&files);

    // Create a new hashmap
    let mut hash_map: HashMap<String, String> = HashMap::new();
    // Accumaltor for increment.
    let mut accumulator = 0;

    // Match statement to check result of hash.
    match hashes {
        Ok(hashes) => {
            // If Ok, iterate through and insert name of file
            // and hash into a hashmap.
            for hash in hashes {
                let index = &files[accumulator];
                hash_map.insert(index.to_string(), hash);

                // Increment accumaltor to move onto next file in vector.
                accumulator += 1;
            }
        }
        // Produce an error if hashing failed.
        Err(error) => eprintln!("Error: {}", error),
    }

    // Don't create file if check is true.
    // Default is false (bool)
    if args.check == false {
        // Create JSON file from data.
        let _ = create_json_file(&output_file, hash_map);

        // Check if the file has been created.
        if check_file_created(&output_file) {
            println!("[*] Success. File created at: {}", &output_file);
        }
    } else {
        // Call function to instead check original hashes against 'new' calculated ones.
        check_integrity::compare(original_hashes, hash_map);
    }
}

fn check_if_directory_valid(directory: String) -> bool {
    // Check if result is Ok and return bool
    if let Ok(result) = fs::metadata(directory) {
        // Check if it is a directory.
        result.is_dir()
    } else {
        false
    }
}

fn get_files_in_folder(folder: String) -> Vec<String> {
    // Take folder parameter, return iterator over entries in dir.
    let paths = fs::read_dir(folder).unwrap();

    let file_paths: Vec<String> = paths
        .filter_map(|item| item.ok())
        // Filter non-files
        .filter(|item| item.path().is_file())
        // Convert each entry to a string
        .map(|entry| entry.path().to_str().map(String::from))
        .flatten()
        // Collect into vector
        .collect();

    file_paths
}

fn hash_files(files: &Vec<String>) -> Result<Vec<String>> {
    // Create new hash vector.
    let mut hashes_vector = Vec::new();

    // Iterate over files.
    for file in files {
        // Open the file.
        let mut fpointer = File::open(file)?;

        // New vector to store the file contents.
        let mut file_contents = Vec::new();

        // Read file contents into memory.
        fpointer.read_to_end(&mut file_contents)?;

        // Create a new SHA256 hasher.
        let mut hasher = Sha256::new();

        // Feed the hasher with the file contents.
        hasher.update(&file_contents);

        // Procure the final hash value
        let hash_as_hex = hex::encode(hasher.finalize());

        // Push the hash onto the vector.
        hashes_vector.push(hash_as_hex);
    }

    // Return a result of Vec<String> (hashes)
    Ok(hashes_vector)
}

// Code adapted from:
// https://stackoverflow.com/questions/69449293/how-to-write-a-vector-to-a-json-file
fn create_json_file(filename: &String, hash_map: HashMap<String, String>) -> Result<()> {
    // Create file from filename
    let output_file = File::create(filename).expect("[!] Error. Failed to create file.");

    // Create new buffer writer and write to output file.
    let mut writer = BufWriter::new(output_file);

    // Write to json file.
    serde_json::to_writer(&mut writer, &hash_map)?;

    // Take date from buffer and write to the file
    writer.flush().expect("[!] Error. Failed to flush buffer.");
    Ok(())
}

fn check_file_created(filename: &String) -> bool {
    let file_metadata = fs::metadata(filename);
    let mut is_file = false;

    match file_metadata {
        Ok(path) => {
            if path.is_file() {
                is_file = true;
            }
        }
        Err(error) => eprintln!("[!] Error. {}", error),
    }

    is_file
}
