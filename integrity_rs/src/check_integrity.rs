use colored::Colorize;
use serde_json::{from_str, Value};
use std::{
    collections::HashMap,
    fs,
    fs::File,
    io::{BufWriter, Read, Result, Write},
    process::exit,
};

// Function to parse JSON from file.
// Will be used to compare as original hashes to "new" hashes to check integrity.
pub fn parse_json(json_file: String) -> HashMap<String, String> {
    // Open json file.
    let mut file = File::open(json_file).expect("[!] Error. Failed to open file.");

    // Create new empty string.
    let mut json_as_string = String::new();

    // Read entire file to string; handle errors with .expect()
    file.read_to_string(&mut json_as_string)
        .expect("[!] Error. Failed to read to JSON string.");

    // Parse the json using serde_json
    let json_parsed: Value = from_str(&json_as_string).expect("[!] Error. Failed to parse JSON.");

    // Create new empty hashmap.
    let mut hashes_from_json: HashMap<String, String> = HashMap::new();

    // Error string for eprintln!() macro.
    let error: &str = "[!] Error. Invalid type. Unable to parse.";

    // Dynamically add key and value to hashmap from file.
    if let Value::Object(map) = json_parsed {
        for (key, value) in map {
            match value {
                // Handle all possible data types.
                Value::String(val) => {
                    hashes_from_json.insert(key, val.to_string());
                }
                // Print error for types other than String.
                Value::Bool(_) => eprintln!("{}", &error),
                Value::Number(_) => eprintln!("{}", &error),
                Value::Array(_) => eprintln!("{}", &error),
                Value::Object(_) => eprintln!("{}", &error),
                Value::Null => eprintln!("{}", &error),
            }
        }
    }

    hashes_from_json
    // println!("Hashmap: {:?}", hashes_from_json);
    // exit(0);
}

pub fn compare(original_hashes: HashMap<String, String>, calc_hashes: HashMap<String, String>) {
    // Create counter to count number of modified files.
    let mut modified_counter = 0;
    let ok = "is ok.".green().bold();
    let modified = "was modified.".red().bold();

    let header = "[ File list ]".cyan().bold();
    let separator = "================";
    println!("{}\n{}", header, separator);

    // Iterate through each key and check if the values are the same.
    for (key, value) in &original_hashes {
        if let Some(hash) = calc_hashes.get(key) {
            // If the value is the same, print status.
            if hash == value {
                println!("File: {}, Hash: {} {}", key.blue(), value.yellow(), ok);
            } else {
                // Else print status (modified)
                println!(
                    "File: {}, Hash: {} {}",
                    key.blue(),
                    value.yellow(),
                    modified
                );
                // Increment counter.
                modified_counter += 1;
            }
        }
    }

    let summary = "[Summary] ".cyan().bold();
    let all_ok = "All ok.".green().bold();
    let not_ok = "Not ok.".red().bold();

    // Short summary at the end.
    if original_hashes == calc_hashes {
        println!("\n{} {}", summary, all_ok);
    } else {
        // Includes how many files have been modified.
        println!(
            "\n{} {} {} file(s) modified.",
            summary, not_ok, modified_counter
        );
    }

    // Exit to stop program from continuing.
    exit(0);
}
