// File: distro_vectors.rs

pub fn create_arch_distro_vector() -> Vec<&'static str> {
    let arch_distro_list = vec![
        "arch",
        "manjaro",
        "endeavouros",
        "artix",
        "arcolinux",
        "garuda",
        "anarchy",
        "parabola",
        "archbang",
        "architect",
        "blackarch",
        "archlabs",
        "rebornos",
        "arcolinuxd",
        "archstrike",
        "hyperbola",
        "swagarch",
        "archman",
        "chakra",
        "archex",
    ];

    arch_distro_list
}

pub fn create_debian_distro_vector() -> Vec<&'static str> {
    let debian_distro_list = vec![
        "ubuntu",
        "linux mint",
        "debian",
        "elementary",
        "pop!",
        "kali",
        "zorin",
        "mx",
        "deepin",
        "antix",
        "xubuntu",
        "lubuntu",
        "kubuntu",
        "backbox",
        "pureos",
        "parrot security",
        "lmde (mint debian edition)",
    ];

    debian_distro_list
}

