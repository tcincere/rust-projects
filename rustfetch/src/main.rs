mod distro_vectors;

use clap::Parser;
use colored::*;
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::process;
use std::{env, fs};
use std::process::exit;
use crate::distro_vectors::create_arch_distro_vector;

const OS_RELEASE: &str = "/etc/os-release";
const CPU_INFO: &str = "/proc/cpuinfo";
const UPTIME: &str = "/proc/uptime";
const KERNEL_VERSION: &str = "/proc/version";
const MEMORY: &str = "/proc/meminfo";
const HOSTNAME: &str = "/etc/hostname";
const ARCH_PKG_DIRECTORY: &str = "/var/lib/pacman/local";

// TODO: Add ability to count number of packages on debian (apt) based systems.
const DEB_PKG_INFO: &str = "/var/lib/dpkg/status";

#[derive(Parser)]
struct Arguments {
    /// Show pretty output
    #[arg(short, long, default_value_t = false)]
    pretty: bool,
}

fn main() {
    fmt_header();

    let args: Arguments = Arguments::parse();

    // Will check if true, if so call function.
    // Function calls exit(1) so rest of the program does not execute.

    if args.pretty == true {
        print_output_pretty();
    }

    //Print user
    let env_vars = get_environment_variables();
    println!(
        "{} {} (@{}) using {}",
        fmt_title("User"),
        env_vars[0],
        get_hostname(),
        env_vars[1]
    );

    //Print distro
    let distro = get_os();
    println!("{} {}", fmt_title("Distro"), distro);

    // Check if arch distro. If arch, print pacman package count
    if check_if_arch_distro(distro_vectors::create_arch_distro_vector(), &distro) {
        println!("{} {} {}", fmt_title("Pkgs"), get_arch_package_count(), "(pacman)");
    }

    // Print Kernel
    println!("{} {}", fmt_title("Kernel"), get_kernel_version());

    // Print CPU
    let cpu_info = get_cpu_model();
    println!(
        "{} {} ({} cores)",
        fmt_title("CPU"),
        &cpu_info[0],
        &cpu_info[1].trim()
    );

    // Print GPU
    println!("{} {}", fmt_title("GPU"), get_gpu_information());

    //Print memory information
    let mem_in_gb = get_memory_usage();
    let used_mem = mem_in_gb[0] - mem_in_gb[1];
    println!(
        "{} {:.2}GB/{:.2}GB free (used {:.2}GB)",
        fmt_title("Memory"),
        mem_in_gb[1],
        mem_in_gb[0],
        used_mem
    );

    //Print terminal.
    println!("{} {}", fmt_title("Terminal"), env_vars[2]);

    // Print uptime.
    let uptime = get_uptime();
    let uptime_in_hours = uptime_in_hours(uptime);

    println!(
        "{} {:.2} minutes ({:.1} hours)",
        fmt_title("Uptime"),
        &uptime,
        &uptime_in_hours
    );

    // Print rustfetch path (where it is executed from)
    println!("{} {}", fmt_title("Program path"), get_rustfetch_path());

    // Print current working directory.
    let pwd = get_current_working_dir();

    match pwd {
        Some(cwd) => println!("{} {}", fmt_title("Current Dir"), cwd),
        None => eprintln!("[!] Error. Failed to get working directory"),
    }
}

fn fmt_header() {
    println!("  << {} >>", "Rustfetch".red().bold());
}

fn fmt_title(title: &str) -> String {
    title.red().bold().to_string()
}

fn print_output_pretty() {
    let env_vars = get_environment_variables();
    let hostname = get_hostname();
    let distro = get_os();
    let kernel = get_kernel_version();

    let uptime = get_uptime();
    let uptime_in_hours = uptime_in_hours(uptime);

    println!("{}\t-> {} (@{})", fmt_title("User"), env_vars[0], hostname);
    println!("{}\t-> {}", fmt_title("Distro"), distro);

    // Check if arch distro. If arch, print pacman package count
    if check_if_arch_distro(distro_vectors::create_arch_distro_vector(), &distro) {
        println!("{}\t-> {} {}", fmt_title("Pkgs"), get_arch_package_count(), "(pacman)");
    }

    println!("{}\t-> {}", fmt_title("Kernel"), kernel);
    println!("{}\t-> {}", fmt_title("Shell"), env_vars[1]);
    println!(
        "{}\t-> {:.2} mins, ({:.1} hrs)",
        fmt_title("Uptime"),
        uptime,
        uptime_in_hours
    );

    std::process::exit(0);
}

fn get_arch_package_count() -> i32 {
    // Get directory entries
    let dir_entries = match fs::read_dir(ARCH_PKG_DIRECTORY) {
        Ok(entries) => entries,
        Err(_) => {
            // Print error and exit
            eprintln!("[ERROR] Unable to read directory {}", ARCH_PKG_DIRECTORY);
            exit(1);
        }
    };

    let mut package_number = 0;

    // Count number of directories within CONST ARCH_PKG_DIRECTORY
    for entry in dir_entries {
        if let Ok(entry) = entry {
            // Check entry file type - Make sure it is a dir.
            if entry.file_type().map(|filetype| filetype.is_dir()).unwrap_or(false) {
                // If directory, increment package number by 1.
                package_number += 1;
            }
        }
    }

    // Return package number
    package_number
}

fn check_if_arch_distro(arch_distros: Vec<&str>, distro_to_check: &str) -> bool {

    let mut distro_is_arch_based = false;

    // Iterate over all entries
    for entry in arch_distros.iter() {

        // Change to lowercase, check substring match
        if distro_to_check.to_lowercase().contains(entry) {
            distro_is_arch_based = true;
        }
    }

    // Return bool
    distro_is_arch_based
}

fn get_os() -> String {
    let os = read_file_and_search_string(OS_RELEASE, "PRETTY_NAME");
    let parsed = parse_string(&os, "=");

    parsed
}

fn get_gpu_information() -> String {
    // Run command using std::process (lspci)
    // to list PCI devices on the system.
    let lspci_output = std::process::Command::new("lspci")
        .output()
        .expect("[!] Error. Failed to run command");

    // Convert to string.
    let lspci_stdout = String::from_utf8_lossy(&lspci_output.stdout);

    // Create a new string.
    let mut gpu = String::new();

    // Iterate through lspci_stdout and push line which contains "VGA"
    // This will most likely be a graphics card.
    for line in lspci_stdout.lines() {
        if line.contains("VGA") {
            gpu.push_str(&line);
        }
    }

    // Split string based on ":" colon delimiter.
    let mut split_string_on_colon = gpu.split(":");

    // Skip first and second slices between colon.
    // VGA pre-amble.
    split_string_on_colon.next();
    split_string_on_colon.next();

    let mut result = String::new();

    // Match on the rest of the string.
    match split_string_on_colon.next() {
        Some(gpu_device) => result.push_str(gpu_device.trim()),
        None => result.push_str("[!] Error. Couldn't obtain GPU information."),
    }

    result
}

fn get_current_working_dir() -> Option<String> {
    // Result returns a PathBuf type; thus the need to convert it to a &str type.
    match env::current_dir() {
        Ok(current_directory) => {
            // Attempts to convert path into a string.
            if let Some(path_as_string) = current_directory.to_str() {
                Some(path_as_string.to_string())
            } else {
                None
            }
        }
        Err(error) => {
            // Print an error if directory is not obtained.
            eprintln!("[!] Error. Cannot obtain working directory {}", error);
            None
        }
    }
}

fn get_environment_variables() -> Vec<String> {
    // Environment variable key.
    let environment_keys = ["USER", "SHELL", "TERM"];

    // Create a new, mutable vector.
    let mut environment_variables: Vec<String> = Vec::new();

    // Iterate through keys and check if the key exists.
    // If the key exists, push the value to the vector.
    // If not, print an error.
    for key in environment_keys.iter() {
        match env::var(key) {
            Ok(value) => environment_variables.push(value),
            Err(error) => eprintln!(
                "[!] Error. Environment variable unavailable {} {}.",
                key, error
            ),
        }
    }

    environment_variables
}

fn get_cpu_model() -> Vec<String> {
    // Read lines from file and covert each line into vector.
    let cpu = read_lines_from_file(CPU_INFO);

    // Take 4th and 12th index (5 and 13th line)
    let cpu_pretty = &cpu[4];
    let cpu_cores = &cpu[12];

    // Parse each line to obtain only the value stored.
    let cpu_parsed: String = parse_string(cpu_pretty, ":");
    let cpu_cores: String = parse_string(cpu_cores, ":");

    // Return a vector of strings which holds the two values
    // (model name and core count)
    vec![cpu_parsed, cpu_cores]
}

fn get_rustfetch_path() -> String {
    let rust_fetch_pathfile = format!("/proc/{}/cmdline", process::id());
    let rustfetch_path = read_lines_from_file(&rust_fetch_pathfile);
    rustfetch_path[0].to_string()
}

fn get_hostname() -> String {
    let hostname = read_lines_from_file(HOSTNAME);
    hostname[0].to_string()
}

fn get_memory_usage() -> Vec<f32> {
    // Read from /proc/meminfo
    let memory = read_lines_from_file(MEMORY);

    // Split each string into a vector.
    let mem_total = split_string_into_vector(&memory, 0);
    let mem_free = split_string_into_vector(&memory, 1);

    let mem_total = &mem_total[1];
    let mem_free = &mem_free[1];

    // Parse each number as a f32.
    let mem_total_as_int = mem_total.parse::<f32>().unwrap();
    let mem_free_as_int = mem_free.parse::<f32>().unwrap();

    // Return the numbers as a vector.
    kb_to_gb(vec![mem_total_as_int, mem_free_as_int])
    // memory_information_as_gb
}

fn kb_to_gb(memory_stats: Vec<f32>) -> Vec<f32> {
    // Create a new vector (mut)
    let mut memory_stats_in_gb = Vec::new();

    //Covert kb into gb by dividing by 1000000.0
    // Then push each value onto the newly created vector.
    for stat in memory_stats.iter() {
        let result = stat / 1000000.0;
        memory_stats_in_gb.push(result);
    }

    // Return the vector.
    memory_stats_in_gb
}

fn parse_string(unparsed_string: &String, delimiter: &str) -> String {
    // Function to parse a string and collect only values.
    // Create new vector.
    let mut parsed_string = Vec::new();

    // Split the string based on a delimiter.
    let split_string = unparsed_string.split(delimiter);

    for value in split_string {
        // Each value is converted to a string and then pushed to the new vector.
        parsed_string.push(value.to_string());
    }

    // Remove any quotation marks and convert it to a string.
    // Return.
    parsed_string[1].trim_matches('"').to_string()
}

fn read_lines_from_file(file: &str) -> Vec<String> {
    // Open file through parameters and handle error.
    let file = File::open(file).expect("[!] Unable to open file.");
    // Create a new buffer (good for larger files).
    let buffer = BufReader::new(file);

    // For each line collect them into a vector of strings.
    buffer
        .lines()
        .map(|line| line.expect("[!] Unable to read line."))
        .collect()
}

fn split_string_into_vector(string: &Vec<String>, index: usize) -> Vec<String> {
    // Take the string index and convert it to string.
    // Each index will be the line of the file.
    let string_pretty = string[index].to_string();

    // Create a new vector (mut)
    let mut string_vec = Vec::new();

    // For every byte (char), split via whitespace.
    // Ensure the whole line is split into seperate words/characters.
    for byte in string_pretty.split_whitespace() {
        // Push each word/char onto the vector.
        string_vec.push(byte.to_string());
    }

    // Return the vector.
    string_vec
}

fn get_kernel_version() -> String {
    let kernel_lines = read_lines_from_file(KERNEL_VERSION);

    // Split single string into vector of words from string.
    let kernel_lines_split = split_string_into_vector(&kernel_lines, 0);

    // Return third value (kernel release)
    kernel_lines_split[2].to_string()
}

fn get_uptime() -> i32 {
    let uptime = read_lines_from_file(UPTIME);

    // Split single string into vector of words from string.
    let uptime_new_vec = split_string_into_vector(&uptime, 0);

    // Take first value (uptime in seconds)
    let number_of_secs: String = uptime_new_vec[0].to_string();

    // Parse as float32
    let number_of_secs = number_of_secs.parse::<f32>().unwrap();

    // Convert the seconds to minutes and return minutes
    let minutes: i32 = seconds_to_minutes(number_of_secs);
    minutes
}

fn uptime_in_hours(uptime_in_minutes: i32) -> f32 {
    // Make uptime f32 then divide by 60.
    uptime_in_minutes as f32 / 60.0
}

fn seconds_to_minutes(seconds: f32) -> i32 {
    //Convert seconds to minutes
    let time = seconds / 60.0;

    //Round and return as i32 integer
    time.round() as i32
}

fn read_file_and_search_string(file: &str, needle: &str) -> String {
    // Reads file and stores in memory as string.
    let os_file_as_string = fs::read_to_string(file);

    // Create new mutable string.
    let mut test = String::new();

    // Match and call grep_line function if result is Ok().
    // Takes the result and needle and iterates through line by line.
    // If needle exists, stores in variable.
    match os_file_as_string {
        Ok(result) => {
            test = grep_line_in_string(&result, &needle);
        }
        // Else error is printed.
        Err(error) => println!("[!] Error. Can't grep line {}", error),
    }

    test
}

fn grep_line_in_string(contents: &str, needle: &str) -> String {
    // Create new mutable string.
    let mut result = String::new();

    // Iterates through contents, line by line.
    for line in contents.lines() {
        // if line contains needle, push to mut string.
        if line.contains(&needle) {
            result.push_str(&line);
        }
    }

    // Return the result.
    result
}
