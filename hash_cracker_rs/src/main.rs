use hex::ToHex;
use md5;
use sha1::Digest;
use std::{
    env,
    error::Error,
    fs::File,
    io::{BufRead, BufReader},
    time::Instant,
};
use sha2::{Sha256,Digest as otherDigest};

// Define length of constant variables (size of digests)
const SHA1_DIGEST_LENGTH: usize = 40;
const MD5_DIGEST_LENGTH: usize = 32;
const SHA256_DIGEST_LENGTH: usize = 64;
const HASH_INVALID_ERROR: &str = "Supplied hash is invalid";

fn main() -> Result<(), Box<dyn Error>> {
    // A Vector is an array type which can be resized.
    let args: Vec<String> = env::args().collect();

    // println!("The array is: {:#?}",& args);

    // Checking for number of arguments.
    if args.len() != 4 {
        println!("Usage:");
        println!("Hash Cracker: <wordlist.txt> <hash_type> <hash>");
        return Ok(());
    }

    // Remove whitespace from hash (field 2)
    let hash_type: &str = args[2].trim();
    let hash = args[3].trim();
    let hash_length = hash.len();

    // Check if hash meets digest length; else return an error.
    if hash_type == "MD5" && hash_length != MD5_DIGEST_LENGTH {
        return Err(HASH_INVALID_ERROR.into());
    }

    if hash_type == "SHA1" && hash_length != SHA1_DIGEST_LENGTH {
        return Err(HASH_INVALID_ERROR.into());
    }

    if hash_type == "SHA256" && hash_length != SHA256_DIGEST_LENGTH {
        return Err(HASH_INVALID_ERROR.into());
    }
    println!("HASH: {}, type <{}>", &hash, &hash_type);

    // Create variable which stores reference to file.
    let wordlist = File::open(&args[1])?;
    let reader = BufReader::new(&wordlist);

    // Measure time taken.
    let time = Instant::now();

    for line in reader.lines() {
        // Trim and convert each line to string.
        let line = line?;

        let common_password = line.trim();
        let calc_hash;

        // Use match instead of if/else - stylistic reasons.
        match hash_type {
            "SHA1" => calc_hash = hash_sha1(&common_password),
            "MD5" => calc_hash = hash_md5(&common_password),
            "SHA256" => calc_hash = hash_sha256(&common_password),
            _ => calc_hash = "Null".to_string(),
        }

        if hash == calc_hash {
            println!("\nCRACKED: {}\n\nTime Elapsed ({:.2?})", &common_password, time.elapsed());
            return Ok(());
        } else {
            continue;
        }
    }

    Ok(())
}

fn hash_md5(common_password: &str) -> String {
    let calc_hash = md5::compute(common_password.as_bytes());
    calc_hash.encode_hex()
}

fn hash_sha1(common_password: &str) -> String {
    let calc_hash = &hex::encode(sha1::Sha1::digest(common_password.as_bytes()));
    calc_hash.to_string()
}

fn hash_sha256(common_password: &str) -> String {
    let calc_hash = Sha256::digest(&common_password.as_bytes());
    calc_hash.encode_hex()
}
