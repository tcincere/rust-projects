use clap::Parser;
use serde::{Deserialize, Serialize};
use std::process;

#[derive(Parser)]
struct Info {
    /// Enter country
    country: String,
    /// Enter city (capitalise first letter)
    city: String,
}

#[derive(Debug, Serialize, Deserialize)]
struct University {
    #[serde(rename = "state-province")]
    state_province: Option<String>,
    name: String,
    domains: Vec<String>,
    web_pages: Vec<String>,
    country: String,
    alpha_two_code: String,
}

#[tokio::main]
async fn main() -> Result<(), reqwest::Error> {
    let args: Info = Info::parse();

    let request_url = format!(
        "http://universities.hipolabs.com/search?country={country}",
        country = args.country
    );

    let response: Vec<University> = reqwest::Client::new()
        .get(request_url)
        .send()
        .await?
        .json()
        .await?;

    let city: String = args.city.to_string();

    if response.len() == 0 {
        println!("Data unavailable for {}, {}.", &city, &args.country);
        process::exit(0);
    }

    println!("\tUniversities in {} ({})\n", &city, &args.country);

    for i in 0..response.len() {
        if response[i].name.contains(&city) {
            println!("{}\n{:?}\n", response[i].name, response[i].web_pages);
        }
    }

    Ok(())
}
