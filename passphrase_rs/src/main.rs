use clap::Parser;
use rand::Rng;
use std::time::Instant;
use voca_rs::Voca;
mod dictionary;

#[derive(Parser)]
struct Arguments {
    ///Number of words to use in passphrase
    #[arg(short, long, default_value_t = 10)]
    length: u32,

    ///Delimiter to seperate each word
    #[arg(short, long, default_value = "-")]
    delimiter: String,

    ///Title case, supply arg if true
    #[arg(short, long, default_value_t = false)]
    title_case: bool,

    ///Number of passphrases to be generated
    #[arg(short, long, default_value_t = 1)]
    quantity: u32,
}

fn main() {
    // Collect user-supplied arguments into var
    let args: Arguments = Arguments::parse();

    // Set quantity counter to 0
    let mut quantity_counter: u32 = 0;

    // Measure time
    let time = Instant::now();

    // Loop to call function equal to quantity
    while quantity_counter < args.quantity {
        generate_passphrase(args.length, args.delimiter.clone(), args.title_case);
        quantity_counter += 1;
    }

    println!(
        "(total generated: {}, took: {:.2?})",
        args.quantity,
        time.elapsed()
    );
}

fn generate_passphrase(length: u32, delimiter: String, title_case: bool) {
    // Create variable where final passphrase will be stored
    let mut pass: String = String::from("");

    // Counter for incrementing number of words to be used & quantity
    let mut counter: u32 = 0;

    while counter != length {
        // Generate a random number from 1 to 360...
        let random_number = rand::thread_rng().gen_range(1..360336);

        // Choose a random index from the WORDS_DIC array
        let random_word = dictionary::WORDS_DIC[random_number];

        // Capitalise first letter of word if true
        if title_case == true {
            let capitalised_word = &random_word._capitalize(true);
            pass.push_str(&capitalised_word);
        } else {
            pass.push_str(&random_word);
        }

        // Push the delimiter after the word
        pass.push_str(&delimiter);
        counter += 1;
    }

    println!("{}\n", &pass);
}
